/*
 * @Author William Trudel*/
package rps;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
	private RpsGame game=new RpsGame();
	public void start(Stage stage) {
		Group root = new Group(); 

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		
		HBox buttons=new HBox();
		Button rock=new Button("rock");
		Button paper=new Button("paper");
		Button scissors=new Button("scissors");
		buttons.getChildren().addAll(rock,paper,scissors);
		
		HBox text=new HBox();
		TextField welcome=new TextField("Welcome!");
		welcome.setPrefWidth(200);
		TextField wins=new TextField("wins: "+game.getWins());
		TextField losses=new TextField("losses: "+game.getLosses());
		TextField ties=new TextField("ties: "+game.getTies());
		text.getChildren().addAll(welcome,wins,losses,ties);
		
		VBox box=new VBox();
		box.getChildren().addAll(buttons,text);
		
		root.getChildren().add(box);
		
		RpsChoice cRock=new RpsChoice(welcome,wins,losses,ties,"rock",game);
		rock.setOnAction(cRock);
		RpsChoice cPaper=new RpsChoice(welcome,wins,losses,ties,"paper",game);
		paper.setOnAction(cPaper);
		RpsChoice cScissors=new RpsChoice(welcome,wins,losses,ties,"scissors",game);
		scissors.setOnAction(cScissors);
		
		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		
		
		stage.show(); 
	}
	public static void main(String[] args) {
		Application.launch(args);
	}

}
