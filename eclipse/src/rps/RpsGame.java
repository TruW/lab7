/*
 * @Author William Trudel*/
package rps;

import java.util.Random;

public class RpsGame {
	Random r=new Random();
	private int wins;
	private int ties;
	private int losses;
	private int pick;
	
	public RpsGame() {
		this.wins=0;
		this.ties=0;
		this.losses=0;
		this.pick=r.nextInt(3);
	}

	public int getWins() {
		return wins;
	}
	public int getTies() {
		return ties;
	}
	public int getLosses() {
		return losses;
	}
	
//	0:rock 1:paper 2:scissors
	public String playRound(String choice) {
		pick=r.nextInt(3);
		switch(pick) {
		case 0:
			switch(choice) {
			case "rock":
				ties++;
				return "You both chose rock, it's a tie";
			case "paper":
				wins++;
				return "Computer chose rock, you win";
			case "scissors":
				losses++;
				return "Computer chose rock, you lose";
			default:
				return "invalid input";
			}
		case 1:
			switch(choice) {
			case "paper":
				ties++;
				return "You both chose paper, it's a tie";
			case "scissors":
				wins++;
				return "Computer chose paper, you win";
			case "rock":
				losses++;
				return "Computer chose paper, you lose";
			default:
				return "invalid input";
			}
		default:
			switch(choice) {
			case "scissors":
				ties++;
				return "You both chose scissors, it's a tie";
			case "rock":
				wins++;
				return "Computer chose scissors, you win";
			case "paper":
				losses++;
				return "Computer chose scissors, you lose";
			default:
				return "invalid input";
			}
		}
	}
}
