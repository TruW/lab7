/*
 * @Author William Trudel*/
package rps;

import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String choice;
	private RpsGame rps;
	
	public RpsChoice(TextField message,TextField wins,TextField losses,TextField ties,String choice,RpsGame rps) {
		this.message=message;
		this.wins=wins;
		this.losses=losses;
		this.ties=ties;
		this.choice=choice;
		this.rps=rps;
	}
	
	@Override
	public void handle(ActionEvent e) {
		message.setText(rps.playRound(choice));
		wins.setText("wins: "+rps.getWins());
		losses.setText("losses: "+rps.getLosses());
		ties.setText("ties: "+rps.getTies());
	}
}
