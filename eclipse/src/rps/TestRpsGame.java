/*
 * @Author William Trudel*/
package rps;

import java.util.Scanner;

public class TestRpsGame {

	public static void main(String[] args) {
		RpsGame rg=new RpsGame();
		String choice="";
		
		while(!choice.equals("done")) {
			System.out.println("enter choice");
			choice=new Scanner(System.in).nextLine();
			System.out.println(rg.playRound(choice));
		}
		
		System.out.println(rg.getWins()+" "+rg.getTies()+" "+rg.getLosses()+" ");
	}

}
